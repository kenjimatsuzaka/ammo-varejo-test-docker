## Introdução

Este projeto trata-se da implementação do teste enviado pela AMMO Varejo para uma vaga no time de Desenvolvimento.

## Pré-requisitos

É necessário que tenha instalado em sua máquina **git**, **docker** e **docker-compose**.

[Instale o GIT](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)

[Instale o DOCKER CE](https://docs.docker.com/install/)

[Instale o DOCKER-COMPOSE](https://docs.docker.com/compose/install/)

Obs: (Para usuários Linux) A fim de facilitar a execução dos comandos a seguir, sugiro que coloque o usuario da máquina no grupo do docker:

### `sudo usermod -aG docker your-user`

Não esqueça de deslogar e logar na máquina.


## Passo 1 - Repositórios

O primeiro passo para rodar o projeto é baixá-lo para sua máquina local.
Os repositórios necessários são: **ammo-varejo-test-docker**, **ammo-varejo-test-frontend** e **ammo-varejo-test-backend**.

Antes de copiar os repositórios, sugiro criar uma pasta em sua máquina para centralizar as pastas do projeto.

### `mkdir knj-test`

### `cd knj-test`

Agora faça o `git clone` de cada um dos módulos:

[ammo-varejo-test-docker](https://bitbucket.org/kenjimatsuzaka/ammo-varejo-test-docker/src)

[ammo-varejo-test-backend](https://bitbucket.org/kenjimatsuzaka/ammo-varejo-test-backend/src)

[ammo-varejo-test-frontend](https://bitbucket.org/kenjimatsuzaka/ammo-varejo-test-frontend/src)


## Passo 2 - Subindo a Aplicação

Neste passo vamos subir a aplicação:

- entre na pasta **ammo-varejo-test-docker**:

### `cd ammo-varejo-test-docker`

- execute o comando:

### `docker-compose up -d`

- tenha certeza de que todos os contêineres foram iniciados e não deram erro:

### `docker-compose ps`

- vamos criar a tabela no Banco de Dados:

### `docker exec -it api sequelize db:migrate`

- vamos popular a tabela:

### `docker exec -it api sequelize db:seed:all`

- basta acessar em seu browser:

### `localhost:4200`


## Passo 3 - Testes

Foram criados alguns testes no backend para garantir que a API esteja funcionando corretamente, e auxiliar no compreendimento do seu comportamento.

Para executar os testes:

- acesse a pasta do repositório **ammo-varejo-test-backend**:

### `cd ../ammo-varejo-test-backend`

- remova os arquivos **sequelizeMeta.json** e **sequelizeData.json** da pasta **config/**:

### `sudo rm -f config/sequelize*`

- acesse o contêiner do Banco de Dados e crie a DataBase **mmartan_test**:

### `docker exec -it db mysql -p -u root`

Obs: a senha é **root**!!!!

### `CREATE DATABASE mmartan_test;`

- saida do contêiner:

### `CTRL+D`

- execute o comando de teste:

Se é a primeira vez:

### `docker exec -it api npm run first-test`

Para as demais vezes:

### `docker exec -it api npm test`

- após o fim da execução, saia com:

### `CTRL+C`


### Passo Bônus - SSR

Foi criado uma versão SSR do frontend, e pode ser encontrado em:

[ammo-varejo-test-ssr](https://bitbucket.org/kenjimatsuzaka/ammo-varejo-test-ssr/src)

Para rodar esta aplicação:

- faça o `git clone` do repositório

- entre na pasta **ammo-varejo-test-docker**:

### `cd ammo-varejo-test-docker`

- execute o comando:

### `docker-compose up -d api db`

- entre na pasta **ammo-varejo-test-ssr**:

### `cd ammo-varejo-test-ssr`

- execute:

### `npm install`

### `npm run build`

### `node bin`

Após este último comando, a aplicação estará rodando em `localhost:3000`


## Passo 5 - Considerações finais

Esta aplicação foi hospedada na AWS (EC2 e RDS) e pode ser acessada pela internet.

[Teste AMMO Varejo na AWS](http://18.228.197.98/)

Pronto! Chegamos ao fim do tutorial de Preparação, Execução e Testes do projeto... :)

Espero que tudo tenha ocorrido como o esperado, e qualquer dúvida ou crítica, pode me falar pessoalmente, quando eu estiver me juntado ao time e estiver sentado aí com vcs! hehehe

Brincadeiras à parte, o meu contato é kenji.matsuzaka@gmail.com, ou se prefirem me ligar, (11)99495-3237

Abs,
Kenji
