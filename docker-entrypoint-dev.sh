#!/bin/sh
set -e
cd /src

# installing project's dependencies
if test -d node_modules; then
    rm -Rf node_modules;
fi

npm install --no-bin-links

# running project
npm run dev
