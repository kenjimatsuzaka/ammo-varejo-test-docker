FROM node:8.15

RUN mkdir /src

RUN npm install pm2 -g

RUN npm install -g sequelize-cli

RUN npm install nodemon -g

RUN npm install mocha -g

RUN npm install ts-node typescript -g

WORKDIR /src

# Setup startup script
COPY docker-entrypoint-dev.sh /docker-entrypoint.sh
RUN chmod +x /docker-entrypoint.sh
ENTRYPOINT ["/docker-entrypoint.sh"]
